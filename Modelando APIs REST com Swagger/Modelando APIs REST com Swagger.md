# Tutorial modelando APIs REST com SWAGGER

## O que é Swagger?

O Swagger é um projeto composto por algumas ferramentas que auxiliam o desenvolvedor de APIs REST em algumas tarefas como:

 - Modelagem da API
 - Geração de documentação (legível) da API
 - Geração de códigos do Cliente e do Servidor, com suporte a várias linguagens de programação

 Para isso, o Swagger especifica a OpenAPI, uma linguagem para descrição de contratos de APIs REST. A OpenAPI define um formato JSON com campos padronizados (através de um JSON Schema) para que você descreva recursos, modelo de dados, URIs, Content-Types, métodos HTTP aceitos e códigos de resposta. Também pode ser utilizado o formato YAML, que é um pouco mais legível e será usado nesse post.

 Além da OpenAPI, o Swagger provê um ecossistema de ferramentas. As principais são:


[Swagger Editor](https://swagger.io/tools/swagger-editor/) – para a criação do contrato.
[Swagger UI](https://swagger.io/tools/swagger-ui/)  – para a publicação da documentação.
[Swagger Codegen](https://swagger.io/tools/swagger-codegen/) – para geração de “esqueletos” de servidores em mais de 10 tecnologias e de clientes em mais de 25 tecnologias diferentes.
[Swagger Editor Online](http://editor.swagger.io/) - versão online do swagger editor.
[Swagger Inspector](http://inspector.swagger.io/) - permite gerar, de maneira fácil e rápida, uma definição do OAS(Open API Specification) a partir de qualquer endpoint da API diretamente do seu navegador.

Nesse tutorial, vamos focar na parte de modelagem de uma API já existente com o Swagger Inspector.

A API selecionada para o exemplo é á [Official Joke API](https://github.com/15Dkatz/official_joke_api), uma API de piadas sobre programação e outras.


## Como realizar os testes

![Swagger inspector](img/Screenshot.png)

O Swagger Inspector pode chamar qualquer API com ou sem restrição CORS e ver as respostas associadas para os endpoints chamados. O legal do Inspector é que ele não tem restrições quanto ao tipo de API que você deseja validar. Isso significa que você pode executar testes simples em APIs baseadas em REST, SOAP e GraphQL sem nenhuma restrição ou curva de aprendizado.


## Gerando a documentação do OpenAPI
![Swagger inspector](img/Screenshot2.png)

A especificação OpenAPI, agora o padrão da indústria para projetar e documentar APIs, está tendo uma adoção massiva para otimizar o desenvolvimento de APIs e melhorar o consumo de APIs. O OpenAPI é a base para qualquer boa documentação da API. Os desenvolvedores sempre tiveram que gerar manualmente o arquivo OpenAPI a partir de sua base de código API existente, o que torna um processo longo e complicado.

O Swagger Inspector permite gerar automaticamente o arquivo OpenAPI para qualquer endpoint que você chamar. No painel esquerdo selecione vários endpoints e consolide sua documentação em um único arquivo OpenAPI por meio de uma coleção.

Documentação OpenAPI gerada: [Oficial Joke Open API](https://app.swaggerhub.com/apis-docs/vitorlc/OfficialJokeAPI/0.1#/)

